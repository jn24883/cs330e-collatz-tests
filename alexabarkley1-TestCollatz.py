#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read_2(self):
        s = "15 2\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 15)
        self.assertEqual(j, 2)
        
    def test_read_3(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 999999)
        
    def test_read_4(self):
        s = "10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 10)

    def test_read_5(self):
        s = "4 5\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 4)
        self.assertEqual(j, 5)
        
        

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1000, 2000) #1, 10 
        self.assertEqual(v, 182) # 20
        
    def test_eval_2(self):
        v = collatz_eval(15 ,2)
        self.assertEqual(v, 20)
        
    def test_eval_3(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)

    def test_eval_4(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)



    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 15, 2, 20)
        self.assertEqual(w.getvalue(), "15 2 20\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 10, 10, 7)
        self.assertEqual(w.getvalue(), "10 10 7\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve_2(self):
        r = StringIO("1 1\n15 2\n10 10\n999999 999998\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n15 2 20\n10 10 7\n999999 999998 259\n")
        
    def test_solve_3(self):
        r = StringIO("15 30\n4500 3000\n100 2000\n2 3\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "15 30 112\n4500 3000 238\n100 2000 182\n2 3 8\n")

    def test_solve_4(self):
        r = StringIO("999999 999999\n10 5\n2001 2000\n13 20\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 999999 259\n10 5 20\n2001 2000 113\n13 20 21\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""

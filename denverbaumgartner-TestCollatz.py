#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_1(self):
        s = "1 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1000)

    def test_read_2(self):
        s = "10 5000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 5000)
    
    def test_read_3(self):
        s = "6 60\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  6)
        self.assertEqual(j, 60)

    def test_read_4(self):
        s = "123 1230\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  123)
        self.assertEqual(j, 1230)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 2000)
        self.assertEqual(v, 182)

    def test_eval_6(self):
        v = collatz_eval(1, 1) # corner test
        self.assertEqual(v, 1)

    def test_eval_7(self):
        v = collatz_eval(10, 1) # corner test
        self.assertEqual(v, 20)

    def test_eval_8(self):
        v = collatz_eval(10, 10) # corner test
        self.assertEqual(v, 7)

    def test_eval_9(self):
        v = collatz_eval(999, 998) # corner test
        self.assertEqual(v, 50)

    def test_eval_10(self):
        v = collatz_eval(500, 499) # corner test
        self.assertEqual(v, 111)

    def test_eval_11(self):
        v = collatz_eval(100, 10) # corner test
        self.assertEqual(v, 119)

    def test_eval_12(self):
        v = collatz_eval(9, 1) # corner test
        self.assertEqual(v, 20)
    def test_eval_13(self): 
        v = collatz_eval(999997, 999998) # corner test
        self.assertEqual(v, 259)
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve_1(self):
        r = StringIO("1000 2000\n1 1\n10 1\n10 10\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1000 2000 182\n1 1 1\n10 1 20\n10 10 7\n")
    
    def test_solve_2(self):
        r = StringIO("999 998\n500 499\n100 10\n9 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999 998 50\n500 499 111\n100 10 119\n9 1 20\n")
    
    # test case to ensure that coverage is 100%, passes newline to ensure that it is not tested
    def test_solve_3(self): 
        r = StringIO("\n")
        w = StringIO()
        collatz_solve(r, w)

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
